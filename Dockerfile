FROM maven:3.6.1-jdk-8
WORKDIR /tmp
COPY pom.xml ./pom.xml
COPY src ./src
RUN mvn clean package
WORKDIR ./target
CMD ["java","-jar","cliente-0.0.1-SNAPSHOT.jar"] 
